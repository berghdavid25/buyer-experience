---
  title: Automated Software Delivery with GitLab
  description: Achieve DevOps Automation - code, build, testing, release automation
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        title: Automated software delivery
        subtitle: Automation essentials for achieving digital innovation, cloud native transformations and application modernization
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Start your free trial
          url: /free-trial/
        secondary_btn:
          text: Questions? Contact us
          url: /sales/
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Image: gitLab for automated software delivery"
    - name: 'side-navigation'
      links:
        - title: Overview
          href: '#overview'
        - title: Capabilities
          href: '#capabilities'
      slot_enabled: true
      slot_offset: 2
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content: 
            - name: 'by-solution-intro'
              data:
                text:
                  highlight: Automated software delivery with GitLab
                  description: will help you adopt cloud native, Kubernetes and multi-cloud with ease, achieve faster velocity with lower failures and improve developer productivity by eliminating repetitive tasks.
            - name: 'by-solution-benefits'
              data:
                title: Why automated software delivery?
                video:
                  video_url: "https://player.vimeo.com/video/725654155"
                is_accordion: false
                items:
                  - icon:
                      name: increase
                      alt: Increase Icon
                      variant: marketing
                    header: Scale your SDLC for cloud native adoption
                    text: Eliminate click-ops, introduce checks and balances essential for cloud native adoption.
                  - icon:
                      name: gitlab-release
                      alt: GitLab Release Icon
                      variant: marketing
                    header: Every change is releasable
                    text: More testing, errors detected earlier, less risk.
                  - icon:
                      name: collaboration
                      alt: Collaboration Icon
                      variant: marketing
                    header: Improve developer experience
                    text: Minimize repetitive tasks, focus on value generating tasks.
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content: 
            - name: 'by-solution-showcase'
              offset: 0
              data:
                title: How to automate your software delivery process
                description: To deliver higher quality applications, developers need a tool that doesn't require constant maintenance. They need a tool they can trust.
                image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
                items:
                  - title: Continuous change management
                    text: Coordinate, share and collaborate across your software delivery team to delivery business value faster
                    list:
                      - Enterprise-ready Source Code Management
                      - "Track every change - code for application, infrastructure, policies, configurations"
                      - Control every change - code owners, approvers, rules
                      - Distributed version control for geographically distributed teams
                    link:
                      text: Learn more
                      href: "/stages-devops-lifecycle/source-code-management/"
                      data_ga_name:
                      data_ga_location: body
                    icon:
                      name: gitlab-cd
                      alt: GitLab CD Icon
                      variant: marketing
                    video: https://www.youtube.com/embed/JAgIEdYhj00?enablesjsapi=1&enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
                  - title: Continuous integration and verification
                    text: Accelerate your digital transformation by building high-quality applications, at scale.
                    list:
                      - Code, build, test automation to incrementally build & test every change
                      - Less risk by detecting errors early
                      - Scale with parallel builds, merge trains
                      - Collaborate across projects with multi project pipeline
                    link:
                      text: Learn more
                      href: "/features/continuous-integration/"
                      data_ga_name:
                      data_ga_location: body
                    icon:
                      name: continuous-integration
                      alt: Continuous Integration Icon
                      variant: marketing
                    video: https://www.youtube.com/embed/ljth1Q5oJoo?enablesjsapi=1&enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
                  - title: On-demand environments with GitOps
                    text: Create repeatable and stable environments by minimize risk of manual infrastructure configurations and click-ops.
                    list:
                      - Automate infrastructure to release faster
                      - Recover from errors faster
                      - Choice of push or pull configurations
                      - Secure Kubernetes cluster access to avoid exposing your cluster
                    link:
                      text: Learn more
                      href: "/solutions/gitops/"
                      data_ga_name:
                      data_ga_location: body
                    icon:
                      name: cog-code
                      alt: Cog Code Icon
                      variant: marketing
                    video: https://www.youtube.com/embed/onFpj_wvbLM?enablesjsapi=1&enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
                  - title: Continuous delivery
                    text: Automate your application release process to make software delivery repeatable and on-demand.
                    list:
                      - Make every change ‘releasable’
                      - Progressively deploy changes to minimize disruption
                      - Get feedback faster by testing changes on a subset of users
                    link:
                      text: Learn more
                      href: "/stages-devops-lifecycle/continuous-delivery/"
                      data_ga_name:
                      data_ga_location: body
                    icon:
                      name: continuous-delivery
                      alt: Continuous Delivery Icon
                      variant: marketing
                    video: https://www.youtube.com/embed/L0OFbZXs99U?enablesjsapi=1&enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
    - name: group-buttons
      data:
        header: 
          text: Explore other ways GitLab can help delivery automation.
          link: 
            text: Explore more solutions
            href: /solutions/
        buttons: 
          - text: Continuous integration
            icon_left: continuous-delivery
            href: /features/continuous-integration/
          - text: Continuous software security
            icon_left: devsecops
            href: /solutions/continuous-software-security-assurance/
          - text: Source code management
            icon_left: cog-code
            href: /stages-devops-lifecycle/source-code-management/
    - name: 'solutions-resource-cards'
      data:
        cards:
          - icon:
              name: ebook
              alt: Ebook icon
              variant: marketing
            event_type: "Solution Brief"
            header: "Guide to Automated Software Delivery"
            link_text: "Read more"
            fallback_image: /nuxt-images/blogimages/nvidia.jpg
            href: "https://learn.gitlab.com/automated-software-delivery/solution-brief-asd?lb_email={{lead.email address}}&utm_medium=other&utm_campaign=autosd&utm_content=autosdpage"
          - icon:
              name: ebook
              alt: Ebook icon
              variant: marketing
            event_type: "Ebook"
            header: "Measure ROI of Automated Software Delivery"
            link_text: "Read ROI considerations"
            fallback_image: /nuxt-images/blogimages/worldline-case-study-image.jpg
            href: "https://page.gitlab.com/autosd-roi.html"
          - icon:
              name: ebook
              alt: Ebook icon
              variant: marketing
            event_type: "Ebook"
            header: "Modernize your CI/CD"
            link_text: "Read more"
            fallback_image: /nuxt-images/blogimages/modernize-cicd.jpg
            href: "/resources/ebook-fuel-growth-cicd/"
            data_ga_name: "Modernize your CI/CD"
            data_ga_location: "body"
          - icon:
              name: gitlab
              alt: GitLab icon
              variant: marketing
            event_type: "Calculator"
            header: "How much is your toolchain costing you?"
            link_text: "Calculate ROI"
            href: "https://about.gitlab.com/calculator/roi"
            fallback_image: /nuxt-images/resources/resources_18.jpg
            data_ga_name: "Calculate ROI"
            data_ga_location: "body"
          - icon:
              name: ebook
              alt: Ebook icon
              variant: marketing
            event_type: "Ebook"
            header: "A beginners guide to GitOps"
            link_text: "Read more"
            href: "https://page.gitlab.com/resources-ebook-beginner-guide-gitops.html"
            fallback_image: /nuxt-images/blogimages/blog-performance-metrics.jpg
          - icon:
              name: ebook
              alt: Ebook icon
              variant: marketing
            event_type: "Ebook"
            header: "10 reasons to adopt GitOps today"
            link_text: "Read more"
            href: "https://page.gitlab.com/gitops-enterprise-ebook.html"
            fallback_image: /nuxt-images/blogimages/gitops-image-unsplash.jpg
          - icon:
              name: ebook
              alt: Ebook icon
              variant: marketing
            event_type: "Ebook"
            header: "Scaled CI/CD"
            link_text: "Read more"
            href: "/resources/scaled-ci-cd/"
            fallback_image: /nuxt-images/blogimages/modernize-cicd.jpg
            data_ga_name: "Scaled CI/CD"
            data_ga_location: "body"
          - icon:
              name: case-study
              alt: Case Study Icon
              variant: marketing
            event_type: "Case Study"
            header: "Achieving GitOps success"
            link_text: "Learn from 3 customers"
            href: "https://learn.gitlab.com/gitops-awereness-man-1/gitops_automation_success"
            fallback_image: /nuxt-images/blogimages/xcite_cover_image.jpg
            data_ga_name: "Scaled CI/CD"
            data_ga_location: "body"
          - icon:
              name: report
              alt: Report Icon
              variant: marketing
            event_type: "Analyst Report"
            header: "GitLab is a forward mover in GigaOm Radar for GitOps"
            link_text: "Read the 2022 GigaOm Radar for GitOps"
            href: "https://page.gitlab.com/resources-report-gigaom-gitops-radar.html"
            fallback_image: /nuxt-images/blogimages/anwb_case_study_image_2.jpg
