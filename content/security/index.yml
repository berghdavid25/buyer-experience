---
  title: 'GitLab Trust Center'
  og_itle: 'GitLab Trust Center'
  description: At GitLab, we're committed to Information Security.
  twitter_description: At GitLab, we're committed to Information Security.
  og_description: At GitLab, we're committed to Information Security.
  components:
    - name: 'solutions-hero'
      data:
        note: 
          - We're committed to Information Security
        title: GitLab Trust Center
        subtitle: It's our mission to be the leading example in security, innovation, and transparency.
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        image:
          image_url: /nuxt-images/security/security-hero.jpeg
          image_url_mobile: /nuxt-images/security/security-hero.jpeg
          alt: "GitLab team members"
          bordered: true
    - name: 'security-resources-feature'
      data:
        copy:
          header: 
            text: We take compliance seriously
          column_size: 7
          description:
            text: |
                  At GitLab, we know how much security, privacy and accessibility matters to our customers and stakeholders.
        resources:
          column_size: 5
          items:
            - text: Learn more about GitLab [security compliance controls](https://about.gitlab.com/handbook/security/security-assurance/security-compliance/sec-controls.html)
              icon:
                name: link
                variant: product
            - text: For detailed information on our Security and Compliance posture, see our [Customer Assurance Package](https://about.gitlab.com/security/cap/)
              icon: 
                name: link
                variant: product
    - name: 'security-compliance-cards'
      data:
        column_size: 6
        cards:
          - image: 
              src: /nuxt-images/security/SOC_NonCPA.jpg
              alt: soc certification logo
            title: SOC Certification
            description: GitLab maintains SOC 2 Type 2 and SOC 3 reports for the Security and Confidentiality Trust Services Criteria for the GitLab software-as-a-service (SaaS) platform.
            link: 
              url: https://www.aicpa.org/interestareas/frc/assuranceadvisoryservices/serviceorganization-smanagement.html
              text: Learn More
              data_ga_name: learn more
              data_ga_location: body
          - image: 
              src: /nuxt-images/security/ISO_27001_Logo.jpg
              alt: iso 27001 logo
            title: ISO/IEC 27001:2013 Certification
            description: GitLab maintains ISO/IEC 27001:2013 certification for the information security management system supporting the GitLab software-as-a-service (SaaS) platform.
            link: 
              url: https://www.schellman.com/certificate-directory?certificateNumber=1652216-2
              text: Learn More
              data_ga_name: learn more
              data_ga_location: body
          - image: 
              src: /nuxt-images/security/vpat.jpg
              alt: vpat compliance
            title: VPAT Compliance
            description: GitLab's Accessibility Conformance Report shows our commitment to maintaining a product where everyone can contribute.
            link: 
              url: https://design.gitlab.com/accessibility/vpat
              text: Learn More
              data_ga_name: learn more
              data_ga_location: body
          - image: 
              src: /nuxt-images/security/gdpr.jpg
              alt: gdpr Compliance
            title: GDPR Compliance
            description: GitLab is compliant with GDPR requirements.
            link: 
              url: https://about.gitlab.com/privacy/privacy-compliance/
              text: Learn More
              data_ga_name: learn more
              data_ga_location: body
          - image: 
              src: /nuxt-images/security/BitSight_Logo_Tagline.jpg
              alt: bitsight security logo
            title: Bitsight Security Rating
            description: GitLab maintains an advanced Bitsight security rating for our production environment.
            link: 
              url: https://about.gitlab.com/blog/2020/12/18/how-gitlab-uses-third-party-security-ratings-to-build-customer-confidence/
              text: Learn More
              data_ga_name: learn more
              data_ga_location: body
          - image: 
              src: /nuxt-images/security/CSA_Trusted_Cloud_Provider_badge.jpg
              alt: csa trusted cloud provider badge
            title: CSA Trusted Cloud Provider
            description: GitLab is a Cloud Security Alliance (CSA) Trusted Cloud Provider.
            link: 
              url: https://cloudsecurityalliance.org/star/registry/gitlab/
              text: Learn More
              data_ga_name: learn more
              data_ga_location: body
    - name: 'security-resources-links'
      data:
        title: Resources
        items:
          - header: Security
            items:
              - text: 'Security FAQs'
                link: https://about.gitlab.com/security/faq/
                data_ga_name: security faqs
                data_ga_location: body
              - text: 'Our security practices'
                link: https://about.gitlab.com/handbook/security/
                data_ga_name: our security practices
                data_ga_location: body
              - text: 'GitLab instance: security best practices'
                link: https://about.gitlab.com/blog/2020/05/20/gitlab-instance-security-best-practices/
                data_ga_name: gitlab instance security best practices
                data_ga_location: body
              - text: 'Security blog'
                link: https://about.gitlab.com/blog/categories/security/
                data_ga_name: security blog
                data_ga_location: body
              - text: 'Security department overview'
                link: https://about.gitlab.com/handbook/security/#security-department
                data_ga_name: security department overview
                data_ga_location: body
          - header: Privacy
            items:
              - text: 'Privacy FAQs'
                link: https://about.gitlab.com/privacy/2021-update-faq/
                data_ga_name: privacy faqs
                data_ga_location: body
              - text: 'Privacy policy'
                link: https://about.gitlab.com/privacy/
                data_ga_name: privacy policy
                data_ga_location: body
              - text: 'Personal data requests'
                link: https://about.gitlab.com/handbook/gdpr/
                data_ga_name: personal data requests
                data_ga_location: body
              - text: 'Responsible disclosure policy'
                link: https://about.gitlab.com/security/disclosure/
                data_ga_name: responsible disclosure policy
                data_ga_location: body
              - text: 'GitLab privacy processes'
                link: https://about.gitlab.com/handbook/legal/privacy/
                data_ga_name: gitlab privacy processes
                data_ga_location: body
          - header: Availability
            items:
              - text: 'Availability FAQs'
                link: https://about.gitlab.com/handbook/engineering/infrastructure/faq/
                data_ga_name: availability faqs
                data_ga_location: body
              - text: 'Monitoring of GitLab.com'
                link: https://about.gitlab.com/handbook/engineering/monitoring/
                data_ga_name: monitoring of gitlab.com
                data_ga_location: body
              - text: 'GitLab.com production architecture'
                link: https://about.gitlab.com/handbook/engineering/infrastructure/production/architecture/
                data_ga_name: gitlab.com production architecture
                data_ga_location: body
              - text: 'GitLab releases'
                link: https://about.gitlab.com/releases/
                data_ga_name: gitlab releases
                data_ga_location: body
              - text: 'Infrastructure department overview'
                link: https://about.gitlab.com/handbook/engineering/infrastructure/
                data_ga_name: infrastructure department overview
                data_ga_location: body
    - name: 'security-cta-section'
      data:
        - title: Reach out to our Security Team if you have questions or concerns.
          icon: 
            name: mail
            slp_color: surface-700
          link:
            text: Contact our Security Team
            url: mailto:customer-assurance@gitlab.com
            data_ga_name: contact our security team
            data_ga_location: body
        - title: Get security release notifications and alerts delivered to your inbox
          icon: 
            name: paper-ariplane
            slp_color: surface-700
          link:
            text: Sign up for security notices
            url: https://about.gitlab.com/company/contact/
            data_ga_name: sign up for security notices
            data_ga_location: body
    - name: 'solutions-cards'
      data:
        title: Security solutions with GitLab
        column_size: 4
        link :
          url: /solutions/
          text: Explore more Solutions
          data_ga_name: soluions explore more
          data_ga_location: body
        cards:
          - title: DevSecOps
            description: GitLab empowers your teams to balance speed and security by automating software delivery and securing your end-to-end software supply chain. 
            icon:
              name: devsecops
              alt: Devsecops Icon
              variant: marketing
            href: /solutions/dev-sec-ops/
            data_ga_name: devsecpps learn more
            data_ga_location: body
          - title: Continuous Software Compliance
            description: Integrating security into your DevOps lifecycle is easy with GitLab.
            icon:
              name: build
              alt: build Icon
              variant: marketing
            href: /solutions/continuous-software-compliance/
            data_ga_name: continuous software compliance learn more
            data_ga_location: body
          - title: Software Supply Chain Security
            description: Ensure your software supply chain is secure and compliant.
            icon:
              name: continuous-delivery
              alt: Continuous Delivery
              variant: marketing
            href: /solutions/supply-chain/
            data_ga_name: software supply chain security learn more
            data_ga_location: body


    